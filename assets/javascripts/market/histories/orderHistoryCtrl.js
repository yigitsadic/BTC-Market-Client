angular.module('market').controller('OrderHistoryCtrl', [
  '$scope', '$cookieStore', '$state', '$http',
  function($scope, $cookieStore, $state, $http) {
    var token = $cookieStore.get('btc-market_token');

    $scope.orders = [];

    $http.get('http://btc-market.herokuapp.com/v1/histories/orders',
    {
      headers: { 'Authorization': 'Token token=' + token }
    })
    .success(
      function(data, status, headers, config) {
        $scope.orders = data.data;

        $scope.cancelOrder = function(id){
          $http.put('http://btc-market.herokuapp.com/v1/orders/' + id + '/cancel', {}, {
            headers: { 'Authorization': 'Token token=' + token }
          })
          .success(function(data, status, headers, config) {
            $state.go('home');
          })
          .error(function(response) { console.log(response); });

        }

      })
    .error( function(response) { $state.go('login'); });

  }
]);
