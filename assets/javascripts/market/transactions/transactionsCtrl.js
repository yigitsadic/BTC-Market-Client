angular.module('market').controller('TransactionsCtrl', [
  '$scope', '$http',
  function($scope, $http) {
    $scope.transactions = [];

    $http.get('http://btc-market.herokuapp.com/v1/currency_transactions', {})
    .success(
      function(data, status, headers, config) {
        $scope.transactions = data.data;
      }).error( function(response) { console.log("Some error happend while loading transactions."); });
  }
]);
