angular.module('market').controller('usernavCtrl', [
  '$scope', '$cookieStore',
  function($scope, $cookieStore) {
    var token = $cookieStore.get('btc-market_token');
    // Signed in?
    $scope.signed_in = (typeof token != 'undefined') ? true : false;
  }
]);
