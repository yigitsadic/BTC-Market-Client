angular.module('static-pages', []);

angular.module('static-pages').controller('AboutCtrl', [
  '$scope',
  function($scope) {}
]);

/*
<btf-markdown>
![alt text](https://gitlab.com/yigitsadic/BTC-Market-API/badges/master/build.svg "build")

[BTC Market API](https://gitlab.com/yigitsadic/BTC-Market-API)

# BTCMarket


Sistem tarafından tanimlanacak para ile Bitcoin alıp satilmasina olanak sağlayacak bir sistem. Bu sayede gerçek bir borsada yatırım yapmak isteyen veyahut alim satim pratiği uygulamak isteyen kullanıcılar için güzel bir deneyim yaratmayı amaçlar.


Geliştirmeye destek, öneriler ve katkılar
-----------------------------------------
Projenin tüm kodları Gitlab'de barinacaktir.
</btf-markdown>
*/
